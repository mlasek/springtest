package com.howtodoinjava.demo.controller;

import java.util.List;

import com.howtodoinjava.demo.components.EmployeeService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.howtodoinjava.demo.model.Employee;

@RestController
public class EmployeeController {
	static Logger log = Logger.getLogger(EmployeeController.class.getName());
	@Autowired
	private EmployeeService service;
	
	@RequestMapping("/")
    public List<Employee> getEmployees() throws Exception {

		Exception e = new NullPointerException("random");

		throw e;
    }

	@RequestMapping("/s")
	public List<Employee> getEmployeesReal() {
		return service.getEmployees();
	}

}
